/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.netbeans.modules.xslt.tmap.model.api.completion.handler;

/**
 *
 * @author lativ
 */
public class XmlElement {

//    public enum XmlElState {
//        COMPLETED,
//        UNKNOWN,
//        UNCOMPLETED;
//    }
    
    private boolean mIsCompleteState = false;

    public void setState(boolean isComplete) {
        if (mIsCompleteState) {
            throw new IllegalStateException("parentTag yet init");
        }
        mIsCompleteState = isComplete;
    }
    
    public boolean getState() {
        return mIsCompleteState;
    }
    
    
    
}
