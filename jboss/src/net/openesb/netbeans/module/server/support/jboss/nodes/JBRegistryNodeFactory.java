package net.openesb.netbeans.module.server.support.jboss.nodes;

import java.util.logging.Logger;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY
 */
public class JBRegistryNodeFactory extends org.netbeans.modules.j2ee.jboss4.nodes.JBRegistryNodeFactory {
    
    private static final Logger LOGGER = Logger.getLogger(JBRegistryNodeFactory.class.getName());
    
    @Override
    public Node getTargetNode(Lookup lookup) {
        Node targetNode = super.getTargetNode(lookup);
        
        targetNode.getChildren().add(new Node[] {
                new JBIItemNode(new JBIChildren(lookup), NbBundle.getMessage(JBIItemNode.class, "LBL_Apps"))});
        
        return targetNode;
    }
}
