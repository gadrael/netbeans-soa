package net.openesb.netbeans.module.server.support.jboss;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URLClassLoader;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.deploy.shared.factories.DeploymentFactoryManager;
import javax.enterprise.deploy.spi.DeploymentManager;
import javax.enterprise.deploy.spi.exceptions.DeploymentManagerCreationException;
import javax.enterprise.deploy.spi.factories.DeploymentFactory;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import static org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory.createJBClassLoader;
import org.netbeans.modules.j2ee.jboss4.JBDeploymentManager;
import org.netbeans.modules.j2ee.jboss4.ide.ui.JBPluginProperties;
import org.netbeans.modules.j2ee.jboss4.ide.ui.JBPluginUtils;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY
 */
public class JBDeploymentFactory extends org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory {
 
    private static final Logger LOGGER = Logger.getLogger(JBDeploymentFactory.class.getName());
    
    public static final String URI_PREFIX = "openesb:jboss-deployer:"; // NOI18N
    
    private static JBDeploymentFactory instance;
    
    /**
     * Mapping of a instance properties to a deployment factory.
     * <i>GuardedBy(JBDeploymentFactory.class)</i>
     */
    private final Map<InstanceProperties, DeploymentFactory> factoryCache =
            new WeakHashMap<InstanceProperties, DeploymentFactory>();

    /**
     * Mapping of a instance properties to a deployment manager.
     * <i>GuardedBy(JBDeploymentFactory.class)</i>
     */
    private final Map<InstanceProperties, JBDeploymentManager> managerCache =
            new WeakHashMap<InstanceProperties, JBDeploymentManager>();
    
    public static synchronized DeploymentFactory create() {
        if (instance == null) {
            instance = new JBDeploymentFactory();
            DeploymentFactoryManager.getInstance().registerDeploymentFactory(instance);

        //    registerDefaultServerInstance();
        }

        return instance;
    }
    
    @Override
    public String getDisplayName() {
        return NbBundle.getMessage(JBDeploymentFactory.class, "SERVER_NAME"); // NOI18N
    }
    
    @Override
    public boolean handlesURI(String uri) {
        if (uri != null && uri.startsWith(URI_PREFIX)) {
            return true;
        }

        return false;
    }
    
    @Override
    public DeploymentManager getDeploymentManager(String uri, String uname, String passwd) throws DeploymentManagerCreationException {
        if (!handlesURI(uri)) {
            throw new DeploymentManagerCreationException(NbBundle.getMessage(org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory.class, "MSG_INVALID_URI", uri)); // NOI18N
        }

        synchronized (org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory.class) {
            InstanceProperties ip = InstanceProperties.getInstanceProperties(uri);
            if (ip != null) {
                JBDeploymentManager dm = managerCache.get(ip);
                if (dm != null) {
                    return dm;
                }
            }

            try {
                DeploymentFactory df = getFactory(uri);
                if (df == null) {
                    throw new DeploymentManagerCreationException(NbBundle.getMessage(org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory.class, "MSG_ERROR_CREATING_DM", uri)); // NOI18N
                }

                String jbURI = uri;
                try {
                    int index1 = uri.indexOf('#'); // NOI18N
                    int index2 = uri.indexOf('&'); // NOI18N
                    int index = Math.min(index1, index2);
                    jbURI = uri.substring(0, index); // NOI18N
                } catch (Exception e) {
                    LOGGER.log(Level.INFO, null, e);
                }

                // see #228619
                // The default host where the DM is connecting is based on
                // serverHost parameter if it is null it uses InetAddress.getLocalHost()
                // which is however based on hostname. If hostname is not mapped
                // to localhost (the interface where the JB is running) we get
                // an excpetion
                if (jbURI.endsWith("as7")) { // NOI18N
                    jbURI = jbURI + "&serverHost=" // NOI18N
                            + (ip != null ? ip.getProperty(JBPluginProperties.PROPERTY_HOST) : "localhost"); // NOI18N
                }
                JBDeploymentManager dm = new JBDeploymentManager(df, uri, jbURI, uname, passwd);
                init(dm);
                
                if (ip != null) {
                    managerCache.put(ip, dm);
                }
                return dm;
            } catch (NoClassDefFoundError e) {
                DeploymentManagerCreationException dmce = new DeploymentManagerCreationException("Classpath is incomplete"); // NOI18N
                dmce.initCause(e);
                throw dmce;
            }
        }
    }
    
    private void init(JBDeploymentManager deploymentManager) {
        Field jbUriField = null;
        boolean accessible = false;
        
        try {
            jbUriField = JBDeploymentManager.class.getDeclaredField("jbUri");
            accessible = jbUriField.isAccessible();
            jbUriField.setAccessible(true);
            
            String jbiUri = (String) jbUriField.get(deploymentManager);
            jbiUri = jbiUri.replaceFirst(JBDeploymentFactory.URI_PREFIX, 
                    org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory.URI_PREFIX);
            jbUriField.set(deploymentManager, jbiUri);
        } catch(Exception ex) {
            LOGGER.log(Level.WARNING, null, ex);
        } finally {
            if (jbUriField != null) {
                jbUriField.setAccessible(accessible);
            }
        }
    }
    
    private DeploymentFactory getFactory(String instanceURL) {
        DeploymentFactory jbossFactory = null;
        try {
            String jbossRoot = InstanceProperties.getInstanceProperties(instanceURL).
                                    getProperty(JBPluginProperties.PROPERTY_ROOT_DIR);

            String domainRoot = InstanceProperties.getInstanceProperties(instanceURL).
                                    getProperty(JBPluginProperties.PROPERTY_SERVER_DIR);

            // if jbossRoot is null, then we are in a server instance registration process, thus this call
            // is made from InstanceProperties creation -> JBPluginProperties singleton contains
            // install location of the instance being registered
            if (jbossRoot == null) {
                jbossRoot = JBPluginProperties.getInstance().getInstallLocation();
            }

            // if domainRoot is null, then we are in a server instance registration process, thus this call
            // is made from InstanceProperties creation -> JBPluginProperties singleton contains
            // install location of the instance being registered
            if (domainRoot == null) {
                domainRoot = JBPluginProperties.getInstance().getDomainLocation();
            }

            InstanceProperties ip = InstanceProperties.getInstanceProperties(instanceURL);
            synchronized (org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory.class) {
                if (ip != null) {
                    jbossFactory = (DeploymentFactory) factoryCache.get(ip);
                }
                if (jbossFactory == null) {
                    JBPluginUtils.Version version = JBPluginUtils.getServerVersion(new File(jbossRoot));
                    URLClassLoader loader = (ip != null) ? getJBClassLoader(ip) : createJBClassLoader(jbossRoot, domainRoot);
                    if(version!= null && "7".equals(version.getMajorNumber())) {
                        Class<?> c = loader.loadClass("org.jboss.as.ee.deployment.spi.factories.DeploymentFactoryImpl");
                        c.getMethod("register").invoke(null);
                        jbossFactory = (DeploymentFactory) c.newInstance();//NOI18N
                    } else {
                        jbossFactory = (DeploymentFactory) loader.loadClass("org.jboss.deployment.spi.factories.DeploymentFactoryImpl").newInstance();//NOI18N
                    }


                    if (ip != null) {
                        factoryCache.put(ip, jbossFactory);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO, null, e);
        }

        return jbossFactory;
    }
}
