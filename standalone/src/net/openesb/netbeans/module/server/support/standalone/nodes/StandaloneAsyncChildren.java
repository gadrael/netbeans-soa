package net.openesb.netbeans.module.server.support.standalone.nodes;

import java.util.concurrent.ExecutorService;
import org.openide.nodes.Children;
import org.openide.util.RequestProcessor;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class StandaloneAsyncChildren extends Children.Keys {

    private static final ExecutorService EXECUTOR = new RequestProcessor(StandaloneAsyncChildren.class);

    public final ExecutorService getExecutorService() {
        return EXECUTOR;
    }
}
