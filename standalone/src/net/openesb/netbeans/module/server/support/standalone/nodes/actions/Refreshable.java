package net.openesb.netbeans.module.server.support.standalone.nodes.actions;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface Refreshable {

    public void updateKeys();
}
